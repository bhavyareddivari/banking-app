package midterm.bhavya;

import java.util.Scanner;

public class Validator {
	
		/**
		 * Method to validate given integer
		 * @param sc
		 * @param prompt
		 * @return
		 */
		public static int getInt(Scanner sc, String prompt)
	    {
	        int i = 0;
	        boolean isValid = false;
	        while (isValid == false)
	        {
	            System.out.print(prompt);
	            if (sc.hasNextInt())
	            {
	                i = sc.nextInt();
	                isValid = true;
	            }
	            else
	            {
	                System.out.println("Error! Invalid integer value. Try again.");
	            }
	            sc.nextLine();  // discard any other data entered on the line
	        }
	        return i;
	    }

	    /**
	     * Method to validate integer within range.
	     * @param sc
	     * @param prompt
	     * @param min
	     * @param max
	     * @return
	     */
	    public static int getIntWithinRange(Scanner sc, String prompt,int min, int max)
	    {
	        int i = 0;
	        boolean isValid = false;
	        while (isValid == false)
	        {
	            i = getInt(sc, prompt);
	            if (i <= min)
	                System.out.println(
	                    "Error! Number must be greater than " + min);
	            else if (i >= max)
	                System.out.println(
	                    "Error! Number must be less than " + max);
	            else
	                isValid = true;
	        }
	        return i;
	    }
	    
	    /**
	     * Method to validate entered string
	     * @param sc
	     * @param prompt
	     * @return
	     */
	    public static String getRequiredString(Scanner sc,String prompt)
	    {
	        String s = "";
	        boolean isValid = false;
	        while (isValid == false)
	        {
	            System.out.print(prompt);
	            s = sc.nextLine();
	            if (s.equals(""))
	            {
	                System.out.println("Error! This entry is required. Try again.");
	            }
	            else
	            {
	                isValid = true;
	            }
	        }
	        return s;
	    }

	    /**
	     * Method to validate string within range
	     * @param sc
	     * @param prompt
	     * @param string1
	     * @param string2
	     * @return
	     */
	    public static String getChoiceString(Scanner sc,String prompt, String string1, String string2)
	    {
	        String s = "";
	        boolean isValid = false;
	        while (isValid == false)
	        {
	            s =getRequiredString(sc,prompt);
	            if (
	                !s.equalsIgnoreCase(string1) &&
	                !s.equalsIgnoreCase(string2)
	                )
	            {
	                System.out.println("Error! Entry must be '"+
	                    string1 +"' or '"+ string2 +"'. Try again.");
	            }
	            else
	            {
	                isValid = true;
	            }
	        }
	        return s;
	    }
  
}
