package midterm.bhavya;


/**
 * Account Interface
 * that implements withdraw and deposit methods
 *
 */
public interface  Account {
	
	public  void withdraw(int amount);
	public  void deposit(int amount);
	
}
