package midterm.bhavya;

import java.text.NumberFormat;
import java.util.Scanner;

public class AccountApp {
	
	//static variables
	private Checking checking=new Checking(1000);
	private Savings savings=new Savings(1000);
    private NumberFormat currency = NumberFormat.getCurrencyInstance();
    private static int flag=0;
    private static final String WITHDRAWAL="W"; 
    private static final String DEPOSIT="D";   
    private static final String CHECKING="C";
    private static final String SAVINGS="S";
    
    
    
    //Main Method
	public static void main(String[] args) {
		System.out.println("Welcome to the Account Application\n");
		AccountApp account=new AccountApp();
		account.calculateAmount();
		System.out.println(account.toString());
	}
	
	/**
	 * This method calculates the amount in checking and 
	 * savings account after withdrawal or deposit.
	 */
	public void calculateAmount(){
		//instance variables
		int amount;
		String choice;
		//scanner
		Scanner sc=new Scanner(System.in);
		//Displaying starting balances of checking and savings account
        System.out.println("Starting Balances\n"+ "Checking: " + currency.format(checking.cAmount)+ "\nSavings: " +currency.format(savings.sAmount));
    	//prompting the user for choice
        System.out.println("\nEnter the transactions for the month");
        do{
        	String transType=Validator.getChoiceString(sc,"\nWithdrawal or Deposit?(w/d): ","w","d");
        	String accountType=Validator.getChoiceString(sc,"Checking or Savings?(c/s): ","c","s");
        	amount=Validator.getIntWithinRange(sc,"Amount?: ",100,10000);
        	//if user prompts to withdraw
        	if(transType.equalsIgnoreCase(WITHDRAWAL)){
        		if(accountType.equalsIgnoreCase(CHECKING)){
        			flag++;
        			if(amount<=checking.getcAmount()-1)
        				checking.withdraw(amount);
        			else
        				System.out.println("Cannot withdraw.The Amount in Checking account is "+ checking.getcAmount()+". Bank fee apply.");
        		}
        		else if(accountType.equalsIgnoreCase(SAVINGS)){
        			if(amount<=savings.getsAmount())
        				savings.withdraw(amount);        	
        			else
        				System.out.println("Cannot withdraw.The Amount in Savings account is "+ savings.getsAmount());
        		}
        	}
        	//if user prompts to deposit
        	else if(transType.equalsIgnoreCase(DEPOSIT)){
        		if(accountType.equalsIgnoreCase(CHECKING)){
            		flag++;
        			checking.deposit(amount);
        		}
        		else if(accountType.equalsIgnoreCase(SAVINGS))
        			savings.deposit(amount);
        	}
        	choice=Validator.getChoiceString(sc,"\nContinue?(y/n): ","y","n");
        }while(choice.equalsIgnoreCase("y"));
        //deducts bankfee when checking account is accessed
        if(flag!=0)
        	checking.finalChecking();
        //calculating interest and final amount for savings
        savings.calculateInterest();
        savings.finalSaving();
        
	}
	
	/* 
	 * Overriding toString() Method
	 */
	@Override
	public String toString() {
		return "\nMonthly Payments and Fees"
				+"\nChecking Fee: "+currency.format(checking.bankFee) 
				+"\nSavings interest payment: "
				+currency.format(savings.sInterest)+"\n"
				+"\nFinal Balances\n" 
				+ "Checking: " +currency.format(checking.getcAmount()) 
				+"\nSavings: "+ currency.format(savings.getsAmount());
				
	}
}

