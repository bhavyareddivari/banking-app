package midterm.bhavya;

/**
 * Checking class which implements Account interface
 * used to perform actions on checking account
 *
 */
public class Checking implements Account{
	
	//instance variables
	int cAmount;
	final int bankFee=1;
	
	/**
	 * Constructor
	 * @param cAmount
	 */
	public Checking(int cAmount){
		this.cAmount=cAmount;
	}
	/**
	 * Getter Method
	 * @return
	 */
	public int getcAmount() {
		return cAmount;
	}
	/**
	 * Setter Method
	 * @param cAmount
	 */
	public void setcAmount(int cAmount) {
		this.cAmount = cAmount;
	}
	
	/* 
	 * overriding method in Account interface
	 * @param amount
	 */
	@Override
	public void withdraw(int amount) {
		cAmount-=amount;
	}
	/* 
	 * overriding method in Account interface
	 * @param amount
	 */
	@Override
	public void deposit(int amount) {
		cAmount+=amount;
	}
	/**
	 *Method to calculate final amount in checking 
	 */
	public void finalChecking(){
    	cAmount-=bankFee;
	}
	
}
