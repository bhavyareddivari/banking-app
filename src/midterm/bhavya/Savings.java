package midterm.bhavya;

/**
 * Savings class which implements Account interface
 * used to perform actions on savings account
 *
 */
public class Savings implements Account{
	
	//Instance variables
	int sAmount;
	int sInterest;
	int interest=1;
	
	/**
	 * Constructor
	 * @param sAmount
	 */
	public Savings(int sAmount){
		this.sAmount=sAmount;
	}
	
	/**
	 * Getter method
	 * @return
	 */
	public int getsAmount() {
		return sAmount;
	}
	/**
	 * setter method
	 * @param sAmount
	 */
	public void setsAmount(int sAmount) {
		this.sAmount = sAmount;
	}
	
	/**
	 * overriding method in Account interface
	 * @param amount
	 */
	@Override
	public void withdraw(int amount) {
		sAmount-=amount;
	}
	/**
	 * overriding method in Account interface
	 * @param amount
	 */
	@Override
	public void deposit(int amount) {
		sAmount+=amount;
	}
	
	/**
	 * Method to calculate interest for savings
	 */
	public void calculateInterest(){
    	sInterest=(sAmount*interest*1)/100;
	}
	
	/**
	 *Method to calculate final amount in savings 
	 */
	public void finalSaving(){
    	sAmount+=sInterest;

	}
}
